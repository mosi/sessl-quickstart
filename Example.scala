import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation with ParallelExecution with CSVOutput {
    model = "./prey-predator.mlrj"
    simulator = SimpleSimulator()
    parallelThreads = -1

    stopTime = 100

    replications = 5

    scan("wolfGrowth" <~ (0.0001, 0.0002))

    observe("s" ~ count("Sheep"))
    observeAt(range(0, 1, 100))

    withRunResult(writeCSV)
  }
}