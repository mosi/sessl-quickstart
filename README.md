# SESSL/ML-Rules Quick Start #

In this repository you find a number of files to quickly get started with simulation experiments with SESSL and ML-Rules.
You do not need to install any additional software, as all required artifacts are downloaded automatically.
The supplied example experiment can be executed with
* `./mvnw scala:script` or the `run.sh` script on Unix
* `mvnw.cmd scala:script` or the `run.bat` script on Windows


To customize the example, you can edit four files:
* Edit the simulation experiment Specification in the `.scala` file or create a new one
* Edit the simulation model in the `.mlrj` file or create a new one
* Change the SESSL version and bindings to use in the `pom.xml`
* Change the log level (i.e., the verbosity of the console output) in the `.mvn/jvm.config`.
Possible values are: `debug`, `info`, `warn`, and `error`.

You can rename and replace the `.mlrj` and `.scala` files. However, make sure that
* the `.scala` file is correctly referenced in the `pom.xml` in the `scriptFile` tag
* the `.mlrj` model file is correctly referenced in the `.scala` file
 
Just see the example for a working configuration.
